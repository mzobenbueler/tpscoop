// Fill out your copyright notice in the Description page of Project Settings.


#include "FireExplosion.h"
#include "Components/SphereComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AFireExplosion::AFireExplosion()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	SphereComp->InitSphereRadius(1.f);
	//SphereComp->SetRelativeLocation(FVector(0.f, 0.f, 20.f));
	SphereComp->SetSimulatePhysics(true);
	SphereComp->SetCollisionProfileName(TEXT("BlockAll"));
	//SphereComp->OnComponentHit.AddDynamic(this, &AMolotovCoktail::OnCompHit);
	//SphereComp->SetNotifyRigidBodyCollision(true);
	RootComponent = SphereComp;

	FireEmitter = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("FireEffect"));
	FireEmitter->SetupAttachment(RootComponent);
	FireEmitter->SetRelativeLocation(FVector(0.f, 0.f, 0.f));
	//static ConstructorHelpers::FObjectFinder<UParticleSystem> FireEmitterAsset(TEXT("/Game/PolygonScifi/Particles/FX_Fire.FX_Fire"));
	//if (FireEmitterAsset.Succeeded())
	//{
	//	FireEmitter->SetTemplate(FireEmitterAsset.Object);
	//}
	FireEmitter->SetRelativeScale3D(FVector::OneVector);

	FireScale = 1.f;
	DelayBeforeDestroyingActor = 1.5f;
	bExplosionEnding = false;


}

// Called when the game starts or when spawned
void AFireExplosion::BeginPlay()
{
	Super::BeginPlay();

	SphereComp->InitSphereRadius(1.f);

	FireEmitter->SetRelativeScale3D(FVector::OneVector * FireScale);

	GetWorldTimerManager().SetTimer(FTimerHandle_BeforeDeactivation, this, &AFireExplosion::EndExplosion, DelayBeforeExtinction, false);

}

void AFireExplosion::EndExplosion()
{
	bExplosionEnding = true;

	FireEmitter->DeactivaateNextTick();

	//Wait for the last flame to be extinguished then destroy actor
	SetLifeSpan(DelayBeforeDestroyingActor);
}

// Called every frame
void AFireExplosion::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireEmitter->SetWorldRotation(FRotator::ZeroRotator);

	if (bExplosionEnding)
	{
		//Stopping the flame continuously
	}

	if (FireEmitter->bWasDeactivated)
	{
		//Destroy();
	}
}

void AFireExplosion::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	UE_LOG(LogTemp, Log, TEXT("NOTIF"));

	float z = UGameplayStatics::ApplyDamage(OtherActor, 50, nullptr, this, FireDamageType);
	UE_LOG(LogTemp, Log, TEXT("Applied : %f"), z);


}
