// Fill out your copyright notice in the Description page of Project Settings.


#include "AICharacter_One.h"

// Sets default values
AAICharacter_One::AAICharacter_One()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AAICharacter_One::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAICharacter_One::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AAICharacter_One::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

