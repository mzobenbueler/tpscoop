// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnCoktailNotifyState.h"
#include "SyntyMixamoCharacter.h"
#include "Engine.h"

void USpawnCoktailNotifyState::NotifyBegin(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation, float TotalDuration)
{
    //Aller chercher le character, puis le Player. 
    if(MeshComp != NULL && MeshComp->GetOwner() != NULL)
    {
        ASyntyMixamoCharacter* Player = Cast<ASyntyMixamoCharacter>(MeshComp->GetOwner());
        if(Player != NULL)
        {
            Player->SpawnThrowableObjectInHandSocket();
        }
    }
}
