// Fill out your copyright notice in the Description page of Project Settings.


#include "ThrowTrajectoryComp.h"
#include "DrawDebugHelpers.h"

// Sets default values for this component's properties
UThrowTrajectoryComp::UThrowTrajectoryComp()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UThrowTrajectoryComp::BeginPlay()
{
	Super::BeginPlay();

	IsTrajectoryEnabled = false;
	
	//Aller chercher la gravite dans GetWorld()->GetGravityZ();
	Gravity = -980.0f;
}


// Called every frame
void UThrowTrajectoryComp::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if(IsTrajectoryEnabled)
	{
		DrawTrajectoryDebugLine();

	}
}

FVector UThrowTrajectoryComp::GetTrajectoryImpulse(FVector StartLocation, FVector EndLocation, float Height)
{
	StartPosition = StartLocation; 
	// Destination = EndLocation;
	// Height = height; 

	LaunchData = CalculateLaunchDataFromLocations(StartLocation, EndLocation, Height );

	return LaunchData.InitialVelocity;

}

float UThrowTrajectoryComp::GetTrajectoryDisplacement(FVector StartLocation, FVector InitialForce, FVector InitialDirection, float Height)
{
	StartPosition = StartLocation; 

	LaunchData = CalculateLaunchDataFromInitialForce(InitialForce, Height);

	return LaunchData.Displacement.X;

}

FLaunchData UThrowTrajectoryComp::CalculateLaunchDataFromInitialForce(FVector InitialForce, float height)
{
	
	float Partie1 =  - 4 * (Gravity/2) * - height;
	float Partie2 = FMath::Square(InitialForce.Z);

	if (Partie2 - Partie1 <= 0 )
	{
		UE_LOG(LogTemp ,Warning,TEXT("Pas assez de force : z %f z %f"), Partie1, Partie2);
	}

	float TimeUp = (-InitialForce.Z + FMath::Sqrt( FMath::Square(InitialForce.Z) - 4 * (Gravity/2) * - height )) / 2 * Gravity / 2;

	float TimeDown = FMath::Sqrt((2 * - height) / Gravity);

	float Time = TimeUp + TimeDown;
 
	float DisplacementX = InitialForce.X * Time;

	FLaunchData data;
	data.InitialVelocity = InitialForce;
	data.TimeToTarget = Time;  
	data.Displacement.X = DisplacementX;
	return data;
}

FLaunchData UThrowTrajectoryComp::CalculateLaunchDataFromLocations(FVector StartLocation, FVector EndLocation, float Height)
{
	//C# float displacementY = target.position.y - ball.position.y;
	float DisplacementZ = EndLocation.Z - StartLocation.Z;

	//C# Vector3 displacementXZ = new Vector3 (target.position.x - ball.position.x, 0, target.position.z - ball.position.z);
	FVector DisplacementXY = FVector(EndLocation.X - StartLocation.X, EndLocation.Y - StartLocation.Y, 0);

	//C# float time = Mathf.Sqrt(-2*h/gravity) + Mathf.Sqrt(2*(displacementY - h)/gravity);
	float Time = FMath::Sqrt(-2 * Height / Gravity) + FMath::Sqrt(2 * (DisplacementZ - Height) / Gravity);

	//C# Vector3 velocityY = Vector3.up * Mathf.Sqrt (-2 * gravity * h);
	FVector VelocityZ = FVector::UpVector * FMath::Sqrt (-2 * Gravity * Height);

	//C# Vector3 velocityXZ = displacementXZ / time;
	FVector VelocityXY = DisplacementXY / Time; 


	//C# return new LaunchData(velocityXZ + velocityY * -Mathf.Sign(gravity), time);
	FLaunchData data;
	data.InitialVelocity = VelocityXY + VelocityZ * - FMath::Sign(Gravity);
	data.TimeToTarget = Time;  
	return data;
}

void UThrowTrajectoryComp::DrawTrajectoryDebugLine()
{
	//C# Vector3 previousDrawPoint = ball.position;
	FVector PreviousDrawPoint = StartPosition;

	int Resolution = 20;
	for (int i = 0; i <= Resolution; i++)
	{
		//C# float simulationTime = i / (float)resolution * launchData.timeToTarget;
		float SimulationTime = i / (float)Resolution * LaunchData.TimeToTarget;

		//C# Vector3 displacement = launchData.initialVelocity * simulationTime + Vector3.up *gravity * simulationTime * simulationTime / 2f;
		FVector Displacement = LaunchData.InitialVelocity * SimulationTime + FVector::UpVector * Gravity * SimulationTime * SimulationTime / 2.f;

		//C# Vector3 drawPoint = ball.position + displacement;
		FVector DrawPoint = StartPosition + Displacement;
		
		//C# Debug.DrawLine (previousDrawPoint, drawPoint, Color.green);
		//DrawDebugLine(GetWorld(), DrawPoint, PreviousDrawPoint, FColor::Black, false, -1.f, 0, 10);		
		DrawDebugSphere(GetWorld(), DrawPoint, 2.f, 4, FColor::Black, false, -1.f, 0, 1.f );

		//C# previousDrawPoint = drawPoint;
		PreviousDrawPoint = DrawPoint;
	}
}

void UThrowTrajectoryComp::SetDisplayTrajectory(bool enabled)
{
	IsTrajectoryEnabled = enabled;

} 
