// Fill out your copyright notice in the Description page of Project Settings.


#include "MolotovCoktail.h"
#include "Components/PrimitiveComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Particles/ParticleSystem.h"
#include "DrawDebugHelpers.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Engine.h"
#include "GameFramework/Actor.h"
#include "FireSphere.h"
//#include "Delegates/DelegateSignatureImpl.inl"
#include "TimerManager.h"

// Sets default values
AMolotovCoktail::AMolotovCoktail()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CapsuleComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsuleComp"));	
    CapsuleComp->InitCapsuleSize(7.5f, 20.f);
	CapsuleComp->SetRelativeLocation(FVector(0.f, 0.f, 20.f));
	CapsuleComp->SetSimulatePhysics(false);
    CapsuleComp->SetNotifyRigidBodyCollision(true);
	CapsuleComp->SetCollisionProfileName(TEXT("NoCollision"));
	CapsuleComp->OnComponentHit.AddDynamic(this, &AMolotovCoktail::OnCompHit);
	RootComponent = CapsuleComp; 

	BottleMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BottleMesh"));
	BottleMesh->SetupAttachment(RootComponent);
	BottleMesh->SetSimulatePhysics(false);
	BottleMesh->SetMassOverrideInKg(NAME_None, 2.f, true);
	BottleMesh->SetCollisionProfileName(TEXT("NoCollision"));
    static ConstructorHelpers::FObjectFinder<UStaticMesh> BottleMeshAsset(TEXT("/Game/PolygonScifi/Meshes/Props/SM_Prop_Bottle_03.SM_Prop_Bottle_03"));
	if(BottleMeshAsset.Succeeded())
	{
		BottleMesh->SetStaticMesh(BottleMeshAsset.Object);
	}

	FireEmitter = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("FireEffect"));
	FireEmitter->SetupAttachment(RootComponent);
	FireEmitter->SetRelativeLocation(FVector(0.0f, 0.0f, 40.0f));
    static ConstructorHelpers::FObjectFinder<UParticleSystem> FireEmitterAsset(TEXT("/Game/PolygonScifi/Particles/FX_Fire.FX_Fire"));
	if(FireEmitterAsset.Succeeded())
	{
		FireEmitter->SetTemplate(FireEmitterAsset.Object);
	}
	FireEmitter->SetRelativeScale3D(FVector::OneVector * 1.f);

	ExplostionEnabled = false;
}

// Called when the game starts or when spawned
void AMolotovCoktail::BeginPlay()
{
	Super::BeginPlay();

	FireSphereActor = AFireSphere::StaticClass();

	ExplostionEnabled = true;
}

// Called every frame
void AMolotovCoktail::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireEmitter->SetWorldRotation(FRotator::ZeroRotator);
}

void AMolotovCoktail::EnableMolotovPhysicsAndThrow(FVector Direction, float Force, FVector RotationDir)
{
	if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Red,TEXT("Explosion Enabled"));

	CapsuleComp->SetCollisionProfileName(TEXT("BlockAllDynamic"));
	CapsuleComp->SetSimulatePhysics(true);
	CapsuleComp->SetAllPhysicsLinearVelocity(FVector::ZeroVector, false);
	CapsuleComp->SetAllPhysicsAngularVelocityInDegrees(RotationDir, false);
	CapsuleComp->AddImpulse(Direction * Force, NAME_None, true);

	ExplostionEnabled = true;
}

void AMolotovCoktail::OnCompHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if(ExplostionEnabled)
	{
		if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Green,TEXT("NEW COLLISION YES"));
		//DrawDebugSphere(GetWorld(), Hit.ImpactPoint, 5, 2, FColor::Red, false, 1.f, 0, 2);

		Explode();

		GenerateFireExplosionFX();
		ExplostionEnabled = false;
	}
}

void AMolotovCoktail::Explode()
{
	TArray<FHitResult> OutHits;	// create tarray for hit results
	TArray<AActor*> SweepActors; // crate tarray for sweep actors
	FVector Start = GetActorLocation();	// start and end locations. The sphere will create the radial sweep.
	FVector End = GetActorLocation();
	FCollisionShape MyColSphere = FCollisionShape::MakeSphere(250.0f);	// create a collision sphere
    //DrawDebugSphere(GetWorld(), GetActorLocation(), MyColSphere.GetSphereRadius(), 50, FColor::Red, false, .5f); // draw collision sphere

	bool isHit = GetWorld()->SweepMultiByChannel(OutHits, Start, End, FQuat::Identity, ECC_WorldStatic, MyColSphere); // check if something got hit in the sweep

	if (isHit)
	{
		if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Green,TEXT("awdawdwdawdawdawd"));

		// loop through TArray
		for (auto& Hit : OutHits)
		{
			UStaticMeshComponent* MeshComp = Cast<UStaticMeshComponent>((Hit.GetActor())->GetRootComponent());
			USkeletalMeshComponent* SkeletalMeshComp = Cast<USkeletalMeshComponent>((Hit.GetActor())->FindComponentByClass<USkeletalMeshComponent>());

			if (MeshComp && MeshComp->Mobility == EComponentMobility::Movable)
			{
				MeshComp->AddRadialImpulse(GetActorLocation(), 500.f, 100.f, ERadialImpulseFalloff::RIF_Constant, true);
				MeshComp->AddImpulse(FVector::UpVector * 2000);

				if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Green,TEXT("Static A POUSSER"));
			} 
			else if (SkeletalMeshComp && SkeletalMeshComp->IsSimulatingPhysics())
			{
				SkeletalMeshComp->AddRadialImpulse(GetActorLocation(), 500.f, 100.f, ERadialImpulseFalloff::RIF_Constant, true);
				SkeletalMeshComp->AddImpulse(FVector::UpVector * 2000);

				if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Green,TEXT("SKELETAL A POUSSER"));
			}
			// else if (SkeletalMeshComp && !SkeletalMeshComp->IsSimulatingPhysics())
			// {
			// 	SkeletalMeshComp->SetSimulatePhysics(true);
			// 	SkeletalMeshComp->AddRadialImpulse(GetActorLocation(), 500.f, 100.f, ERadialImpulseFalloff::RIF_Constant, true);
			// 	SkeletalMeshComp->AddImpulse(FVector::UpVector * 2000);

			// 	if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Green,TEXT("LOOOOL"));
			// }

		}
	}	
}

void AMolotovCoktail::GenerateFireExplosionFX()
{
	
	// FTransform FireTransform = FTransform(FRotator::ZeroRotator, GetActorLocation(), FVector::OneVector * 5.f);
	
	// UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FireEmitter->Template, FireTransform, true, EPSCPoolMethod::None, true);

	// FTransform FireTransform = FTransform(FRotator::ZeroRotator, GetActorLocation(), FVector::OneVector * 5.f);

	// UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FireEmitter->Template, FireTransform, true, EPSCPoolMethod::None, true);

	// FTransform FireTransform = FTransform(FRotator::ZeroRotator, GetActorLocation(), FVector::OneVector * 5.f);

	// UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FireEmitter->Template, FireTransform, true, EPSCPoolMethod::None, true);

	FTimerDelegate TimerDel;
	float valueX = 2.f;
	bool IsLastFlame = false;

	TimerDel.BindUFunction(this, FName("GenerateFireFlame"), valueX, IsLastFlame);
	GetWorldTimerManager().SetTimer(FlameOneTimer, TimerDel, 0.01f, false);

	valueX = 4.f;
	TimerDel.BindUFunction(this, FName("GenerateFireFlame"), valueX, IsLastFlame);
	GetWorldTimerManager().SetTimer(FlameTwoTimer, TimerDel, 0.07f, false);

	valueX = 6.f;
	IsLastFlame = true;
	TimerDel.BindUFunction(this, FName("GenerateFireFlame"), valueX, IsLastFlame);
	GetWorldTimerManager().SetTimer(FlameThreeTimer, TimerDel, 0.20f, false);

	if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Green,TEXT("SPONED"));

	//FireEmitter = CreateDefaultSubobject<UParticleSystemComponent>	
}

void AMolotovCoktail::GenerateFireFlame(float Scale, bool IsLastFlame)
{
	//FTransform FireTransform = FTransform(FRotator::ZeroRotator, GetActorLocation(), FVector::OneVector * Scale);	
	//UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FireEmitter->Template, FireTransform, true, EPSCPoolMethod::None, true);
	
	FVector SpawnLocation = GetActorLocation();
	FRotator SpawnRotation = GetActorRotation();

	AFireSphere* Fire = Cast<AFireSphere>(GetWorld()->SpawnActor(FireSphereActor, &SpawnLocation, &SpawnRotation));
	Fire->SetFlameSize(Scale);

	if(IsLastFlame)
	{
		Destroy();
	}

}