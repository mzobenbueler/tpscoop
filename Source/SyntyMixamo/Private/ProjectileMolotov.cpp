// Fill out your copyright notice in the Description page of Project Settings.

#include "FireExplosion.h"
#include "ProjectileMolotov.h"
#include "Particles/ParticleSystemComponent.h"


AProjectileMolotov::AProjectileMolotov()
{
	ExplosionClass = AFireExplosion::StaticClass();

	FireEmitter = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("FireEffect"));
	FireEmitter->SetupAttachment(RootComponent);
	FireEmitter->SetRelativeLocation(FVector(0.f, 0.f, 0.f));
	//static ConstructorHelpers::FObjectFinder<UParticleSystem> FireEmitterAsset(TEXT("/Game/PolygonScifi/Particles/FX_Fire.FX_Fire"));
	//if (FireEmitterAsset.Succeeded())
	//{
	//	FireEmitter->SetTemplate(FireEmitterAsset.Object);
	//}
	FireEmitter->SetRelativeScale3D(FVector::OneVector * 0.5f);

	DelayBeforeDestroyingActor = 1.5f;

}

void AProjectileMolotov::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::OnHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);

	if (ExplosionClass)
	{
		FVector SpawnLocation = GetActorLocation();
		FRotator SpawnRotation = FRotator::ZeroRotator;

		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		GetWorld()->SpawnActor<AFireExplosion>(ExplosionClass,SpawnLocation, SpawnRotation, SpawnParams);
	}

	//Delete the bottle
	StaticMeshComp->SetVisibility(false);

	FireEmitter->DeactivaateNextTick();

	//Wait for the last flame to be extinguished then destroy actor
	SetLifeSpan(DelayBeforeDestroyingActor);
}