// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/TimelineComponent.h"
#include "Components/PrimitiveComponent.h"
#include "Curves/CurveFloat.h"
#include "DrawDebugHelpers.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/UnrealMathUtility.h"
#include "MolotovCoktail.h"
#include "ThrowTrajectoryComp.h"
#include "ThrowableProjectile.h"
#include "Math/Vector.h"
#include "Math/UnrealMathUtility.h"
#include "SyntyMixamoCharacter.h"
#include "TimerManager.h"
#include "UObject/ConstructorHelpers.h"

//////////////////////////////////////////////////////////////////////////
// ASyntyMixamoCharacter

ASyntyMixamoCharacter::ASyntyMixamoCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	ThrowLocation = CreateDefaultSubobject<USceneComponent>(TEXT("ThrowLocation"));
	ThrowLocation->SetupAttachment(RootComponent);

	ThrowTrajectory = CreateDefaultSubobject<UThrowTrajectoryComp>(TEXT("ThrowTrajectory"));

	ThrowLocation->SetRelativeLocation(FVector(25, 55, 75), false, nullptr, ETeleportType::None);

	/** Create the Trajectory : Moved into constructor*/ 
	//ThrowImpulse = FVector::ZeroVector;
	//FVector SpawnLocation = GetActorLocation();
	//FRotator SpawnRotation = GetActorRotation();
	//ThrowTrajectory = Cast<AThrowableTrajectory>(GetWorld()->SpawnActor(SubClassTrajectory, &SpawnLocation, &SpawnRotation)); THE CAST WAS USED WHEN THE OBJECT WAS AN ACTOR AND NOT A COMPONENT

	/** Load Animation montages */
	#pragma region Load_Animation_Montages
	static ConstructorHelpers::FObjectFinder<UAnimMontage>ThrowObject_Montage_Charge_Object(TEXT("AnimMontage'/Game/Animations/ThrowObject_ChargeMontage.ThrowObject_ChargeMontage'"));
	if(ThrowObject_Montage_Charge_Object.Succeeded())
	{
		ThrowObject_Montage_Charge = ThrowObject_Montage_Charge_Object.Object;
	}

	static ConstructorHelpers::FObjectFinder<UAnimMontage>ThrowObject_Montage_ChargedLoop_Object(TEXT("AnimMontage'/Game/Animations/ThrowObject_ChargedLoopMontage.ThrowObject_ChargedLoopMontage'"));
	if(ThrowObject_Montage_ChargedLoop_Object.Succeeded())
	{
		ThrowObject_Montage_ChargedLoop = ThrowObject_Montage_ChargedLoop_Object.Object;
	}

	static ConstructorHelpers::FObjectFinder<UAnimMontage>ThrowObject_Montage_Release_Object(TEXT("AnimMontage'/Game/Animations/ThrowObject_ReleaseMontage.ThrowObject_ReleaseMontage'"));
	if(ThrowObject_Montage_Release_Object.Succeeded())
	{
		ThrowObject_Montage_Release = ThrowObject_Montage_Release_Object.Object;
	}

	static ConstructorHelpers::FObjectFinder<UAnimMontage>Roll_Forward_Montage_Object(TEXT("AnimMontage'/Game/Animations/Roll/Roll_Forward_Montage.Roll_Forward_Montage'"));
	if(Roll_Forward_Montage_Object.Succeeded())
	{
		RollForward_Montage = Roll_Forward_Montage_Object.Object;
	}

	static ConstructorHelpers::FObjectFinder<UAnimMontage>SprintForward_Montage_Object(TEXT("AnimMontage'/Game/Animations/Sprint/Sprint_Forward_Montage.Sprint_Forward_Montage'"));
	if(SprintForward_Montage_Object.Succeeded())
	{
		SprintForward_Montage = SprintForward_Montage_Object.Object;
	}
	#pragma endregion Load_Animation_Montages

	/* Camera Default values */
	AimingCameraTargetArmLength = 125.f;
	AimingCameraSocketPosition = FVector(0.f, 60.f, 65.f);
	NormalCameraTargetArmLength = 400.f;
	NormalCameraSocketPosition = FVector(0.f, 80.f, 60.f);

	/* Throwable object Default values */
	ThrowableObjectSocketName = "MolotovSocket";
	EquipedProjectile = AThrowableTrajectory::StaticClass();
}

void ASyntyMixamoCharacter::ThrowCurrentProjectile()
{
	if (CurrentProjectile != nullptr)
	{
		CurrentProjectile->DetachFromActor(FDetachmentTransformRules(EDetachmentRule::KeepWorld, false));

		//ThrowProjectile arguments
		float ThrowForce = 1000.f;
		FVector ThrowDirection = FollowCamera->GetForwardVector();
		FRotator ThrowStartRotation = FollowCamera->GetComponentRotation();
		FVector InstigatorVelocity = GetCharacterMovement()->GetLastUpdateVelocity();
		InstigatorVelocity.Normalize();

		CurrentProjectile->ThrowProjectile(ThrowForce, ThrowDirection, ThrowStartRotation, InstigatorVelocity);

		//Delete the pointers once the coktail is thrown
		CurrentProjectile = nullptr;
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void ASyntyMixamoCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &ASyntyMixamoCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASyntyMixamoCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ASyntyMixamoCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ASyntyMixamoCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &ASyntyMixamoCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &ASyntyMixamoCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ASyntyMixamoCharacter::OnResetVR);

	//Attack functionality
	PlayerInputComponent->BindAction("Attack", IE_Pressed, this, &ASyntyMixamoCharacter::Attack);

	//Aim functionality
	PlayerInputComponent->BindAction("Aim", IE_Pressed, this, &ASyntyMixamoCharacter::StartAimMode);
	PlayerInputComponent->BindAction("Aim", IE_Released, this, &ASyntyMixamoCharacter::StopAimMode);

	//Roll
	PlayerInputComponent->BindAction("Roll", IE_Pressed, this, &ASyntyMixamoCharacter::Roll);

	//Sprint
	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &ASyntyMixamoCharacter::SprintStart);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &ASyntyMixamoCharacter::SprintStop);

}

void ASyntyMixamoCharacter::BeginPlay()
{
	Super::BeginPlay();

	/* Spawner Initialization */
	SubClassMolotov = AMolotovCoktail::StaticClass();
	//Should not be necessary anymore
	//SubClassTrajectory = AThrowableTrajectory::StaticClass();

	/* Set Normal Camera mode */
	SetNormalCameraMode();

	/* HEalth and Stamina Initialization */
	FullHealth = 1000.0f;
	Health = FullHealth;
	HealthPercentage = 1.0f;

	//bCanBeDamaged = true; bCanBeDamaget will be set private
	SetCanBeDamaged(true);

	FullStamina = 100.0f;
	Stamina = FullStamina;
	StaminaPercentage = 1.0f;
	PreviousStamina = StaminaPercentage;
	StaminaValue = 0.0f;
	bCanUseStamina = true;

	if(StaminaCurve)
	{
	    FOnTimelineFloat TimelineCallback;
        FOnTimelineEventStatic TimelineFinishedCallback;

        TimelineCallback.BindUFunction(this, FName("SetStaminaValue"));
        TimelineFinishedCallback.BindUFunction(this, FName{ TEXT("SetStaminaState") });

		MyTimeline = NewObject<UTimelineComponent>(this, FName("Stamina Animation"));
        MyTimeline->AddInterpFloat(StaminaCurve, TimelineCallback);
        MyTimeline->SetTimelineFinishedFunc(TimelineFinishedCallback);
		MyTimeline->RegisterComponent();
	}

	bIsAnimationBlended = true;

	/** Create the Trajectory : Moved into constructor*/ 
	ThrowImpulse = FVector::ZeroVector;
	// FVector SpawnLocation = GetActorLocation();
	// FRotator SpawnRotation = GetActorRotation();
	// ThrowTrajectory = Cast<AThrowableTrajectory>(GetWorld()->SpawnActor(SubClassTrajectory, &SpawnLocation, &SpawnRotation));
	

}

void ASyntyMixamoCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	SetDesiredCameraMode(DeltaTime);

	CalculateCharacterAimLocation();

	GetThrowableTrajectory();

	/* Health and Stamina tick*/ 
   	if (MyTimeline != nullptr) MyTimeline->TickComponent(DeltaTime, ELevelTick::LEVELTICK_TimeOnly, nullptr);

	//calcul
	FVector ForwardCam = FollowCamera->GetForwardVector();
	ForwardCam.Z = 0;
	FVector ForwardChar = GetActorForwardVector();
	FRotator RotatorCam = FollowCamera->GetComponentRotation();

	FVector Velocity = GetCharacterMovement()->GetLastUpdateVelocity();
	Velocity.Normalize();

	FRotator LookAt = UKismetMathLibrary::FindLookAtRotation(ForwardCam, ForwardChar);
	
	FQuat LookAtQuat = FQuat::FindBetweenVectors(ForwardCam, ForwardChar);
	
	//UE_LOG(LogTemp, Warning, TEXT("CAM Pos: X : %f, Y : %f, Z : %f"), ForwardCam.X, ForwardCam.Y, ForwardCam.Z);
	//UE_LOG(LogTemp, Warning, TEXT("CAM Rot: X : %f, Y : %f, Z : %f"), RotatorCam.Roll, RotatorCam.Pitch, RotatorCam.Yaw);

	//UE_LOG(LogTemp, Warning, TEXT("CHAR : X : %f, Y : %f, Z : %f"), ForwardChar.X, ForwardChar.Y, ForwardChar.Z);

	//UE_LOG(LogTemp, Warning, TEXT("QUAT : X : %f, Y : %f, Z : %f, W : %f"), LookAtQuat.X, LookAtQuat.Y, LookAtQuat.Z, LookAtQuat.W);






}

void ASyntyMixamoCharacter::CalculateCharacterAimLocation()
{
	FVector CameraPosition = FollowCamera->GetComponentLocation();
	FVector CameraForward = FollowCamera->GetForwardVector(); 
	FVector CameraForwardEnd = (CameraPosition + (CameraForward * 5000.f));

	FVector CharacterPosition = GetCharacterMovement()->GetActorLocation();
	FVector CharacterForward = GetActorForwardVector();
	FVector CharacterForwardEnd = (CharacterPosition + (CharacterForward * 5000.f));
	
	float DistanceBetweenLines = FMath::PointDistToLine(CharacterPosition, CameraForward, CameraPosition);
	float DistanceMaxOnCameraLine = FGenericPlatformMath::Sqrt(FMath::Square(AimDistanceMax) - FMath::Square(DistanceBetweenLines));

	FVector FinalPosition = (CameraPosition + (CameraForward * DistanceMaxOnCameraLine));
	//DrawDebugSphere(GetWorld(), FinalPosition, 5, 2, FColor::Red, false, 0.5, 0, 2);

	//Quand ce point existe et qu'il est assez proche, on peut l'utiliser comme cible
	FHitResult OutHitCam;
	FCollisionQueryParams CollisionParamsCam;
	FVector CameraLineInterruptionPoint; 
	bool CameraLineInterrupted;

	FHitResult OutHitCharacter;
	FCollisionQueryParams CollisionParamsCharacter;
	FVector CharacterLineInterruptionPoint; 
	bool CharacterLineInterrupted;

	if((CameraLineInterrupted = GetWorld()->LineTraceSingleByChannel(OutHitCam, CameraPosition, FinalPosition, ECC_Visibility, CollisionParamsCam)) != NULL)
	{
		CameraLineInterruptionPoint = OutHitCam.ImpactPoint;		
		//DrawDebugSphere(GetWorld(), CameraLineInterruptionPoint, 5, 2, FColor::Blue, false, 0.5, 0, 2);
	}
	if((CharacterLineInterrupted = GetWorld()->LineTraceSingleByChannel(OutHitCharacter, CharacterPosition, FinalPosition, ECC_Visibility, CollisionParamsCharacter)) != NULL)
	{
		CharacterLineInterruptionPoint = OutHitCharacter.ImpactPoint;		
		//DrawDebugSphere(GetWorld(), CharacterLineInterruptionPoint, 5, 2, FColor::Green, false, 0.5, 0, 2);
	}
	
	if(!CameraLineInterrupted && !CharacterLineInterrupted)
	{
		//La cible ne touche aucun objet. Placer un la cible du tir sur le sol juste en dessous de ce point	
		bool FloorFound = false;
		FHitResult OutHitFloor; 
		FVector FloorSearchingEnd = FVector::DownVector * 5000.f; //Mettre la valeur qui devrait etre la valeur maximale
		if((FloorFound = GetWorld()->LineTraceSingleByChannel(OutHitFloor, FinalPosition, FloorSearchingEnd, ECC_Visibility, CollisionParamsCharacter)) != NULL)
		{
			AimLocation = OutHitFloor.ImpactPoint;
		}
		else 
		{
			AimLocation = FinalPosition;
		}

	}
	else if (!CameraLineInterrupted && CharacterLineInterrupted)
	{
		//La ligne venant du joueur est interronpue, mais pas celle de la camera	
		//Ajouter un viseur a L'endroit de l'interruption. Placer la cible a cet endroit	
		//Du coup, cet endroit n'est pas au centre de l'ecran
		//DrawDebugSphere(GetWorld(), CharacterLineInterruptionPoint, 5, 2, FColor::Green, false, -1.f, 0, 2);
		//DrawDebugSphere(GetWorld(), CameraLineInterruptionPoint, 5, 2, FColor::Red, false, -1.f, 0, 2);
		
		AimLocation = CharacterLineInterruptionPoint;
	}
	else if(CameraLineInterrupted && !CharacterLineInterrupted)
	{
		//La ligne venant de la camera est interronpue, mais pas celle du joueur
		//Placer la cible du tir sur la ligne de la camera, a l'endroit de l'interruption, au centre de l'ecran
		//DrawDebugSphere(GetWorld(),CameraLineInterruptionPoint, 5, 2, FColor::Green, false, 0.5, 0, 2);
		//DrawDebugSphere(GetWorld(), CharacterLineInterruptionPoint, 5, 2, FColor::Red, false, 0.5, 0, 2);

		AimLocation = CameraLineInterruptionPoint;
	}
	else if (CameraLineInterrupted && CharacterLineInterrupted)
	{
		//Les deux lignes sont interronpues
		//Arrive tout le temps quand on regarde par terre 
		//Le probleme va a etre si une ligne est interronpue par un mur et l'autre par un mur derriere. 
		//DrawDebugSphere(GetWorld(),CameraLineInterruptionPoint, 5, 2, FColor::Red, false, 0.5, 0, 2);
		//DrawDebugSphere(GetWorld(), CharacterLineInterruptionPoint, 5, 2, FColor::Red, false, 0.5, 0, 2);

		AimLocation = CameraLineInterruptionPoint;
	}
	
	//DrawDebugSphere(GetWorld(), FinalPosition, 5, 2, FColor::Green, false, 0.5, 0, 2);
}

#pragma region Unreal_TPS_Character_Base_Input
void ASyntyMixamoCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ASyntyMixamoCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void ASyntyMixamoCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void ASyntyMixamoCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ASyntyMixamoCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ASyntyMixamoCharacter::MoveForward(float Value)
{
	//UE_LOG(LogTemp, Warning, TEXT("FORWARD PRESSED"));

	if ((Controller != NULL) && (Value != 0.0f))
	{
		//UE_LOG(LogTemp, Warning, TEXT("FORWARD APPLIED : %s"), *FString::SanitizeFloat(Value));

		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ASyntyMixamoCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}
#pragma endregion Unreal_TPS_Character_Base_Input

void ASyntyMixamoCharacter::GetThrowableTrajectory()
{
	if(ThrowTrajectory != nullptr && ThrowLocation != nullptr)
	{
		FVector ThrowPos = ThrowLocation->GetComponentLocation();	
		float dist = FVector::Dist(ThrowPos, AimLocation);
		ThrowImpulse = ThrowTrajectory->GetTrajectoryImpulse(ThrowPos, AimLocation, FMath::Sqrt(dist));

		// ThrowImpulse = FVector(10000.f, 0.f, 10000.f);		
		// FVector Dir = AimLocation - ThrowPos;
		// Dir.Normalize();
		// float DistanceX = ThrowTrajectory->GetTrajectoryDisplacement(ThrowPos, ThrowImpulse, Dir, 250.f);
	}
}

#pragma region Custom_Player_Action_Inputs
void ASyntyMixamoCharacter::StartAimMode()
{
	SetAimingCameraMode();

	ThrowObjectChargeAttack();

	//Prevent to disable character orientation to camera (ex: If an attack without aiming has been prepared)
    GetWorldTimerManager().ClearTimer(DelayBeforeResetingCharacterOrientation);	

	ThrowTrajectory->SetDisplayTrajectory(true);

}

void ASyntyMixamoCharacter::StopAimMode()
{
	SetNormalCameraMode();

	DeleteThrowableObjectFromHandSocket();

	//Prevent to go on ChargedLoop Animation
    GetWorldTimerManager().ClearTimer(ChargingThrowTimer);

	ThrowTrajectory->SetDisplayTrajectory(false);

}

void ASyntyMixamoCharacter::Attack()
{
	/** Verify if there is engough stamina */ 
	if(!FMath::IsNearlyZero(Stamina, 0.001f) && bCanUseStamina)
	{

		AimLocationSelected = AimLocation; 

		//Attack with or without Aiming
		if(bIsAiming)
		{
			ThrowObjectReleaseAttack();
			//The coktail is already equiped, or it is being reloaded
			//Should throw if it is equiped : Only play THROW animation
			//Should do nothing if reloading
		} 
		else
		{
			EnableCharacterRotationTowardCamera();

			ThrowObjectChargeAttack();

			//Delay before throwing the Coktail for real
			GetWorld()->GetTimerManager().SetTimer(DelayBeforeThrowTimer, this, &ASyntyMixamoCharacter::ThrowObjectReleaseAttack, 0.542f, false);
			
			GetWorld()->GetTimerManager().SetTimer(DelayBeforeResetingCharacterOrientation, this, &ASyntyMixamoCharacter::DisableCharacterRotationTowardCamera, 1.f, false);
		} 

		/** Consume stamina - SET AN ACCESSIBLE VALUE HERE */
		SetStaminaChange(-5.0f); 
	}
	else 
	{
		/* Stamina is empty */
		if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Red, TEXT("Stamina : Empty, cannot throw Molotov"));
	}
}

void ASyntyMixamoCharacter::Roll()
{
	bIsAnimationBlended = false;

	PlayAnimMontage(RollForward_Montage, 1.5f, FName("Default"));
}

void ASyntyMixamoCharacter::SprintStart()
{
	if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Blue, TEXT("SPRINT"));

	bIsAnimationBlended = false;

	bIsSprinting = true;

	GetCharacterMovement()->MaxWalkSpeed = 1100;

	//PlayAnimMontage(SprintForward_Montage, 1.f, FName("Default"));
}

void ASyntyMixamoCharacter::SprintStop()
{
	bIsSprinting = false;

	if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Red, TEXT("STOP SPRINT"));
	GetCharacterMovement()->MaxWalkSpeed = 600;
	//StopAnimMontage(SprintForward_Montage);
}
#pragma endregion Custom_Player_Action_Inputs

bool ASyntyMixamoCharacter::GetIsAnimationBlended()
{
	return bIsAnimationBlended;
}

bool ASyntyMixamoCharacter::GetIsSprinting()
{
	return bIsSprinting;
}

bool ASyntyMixamoCharacter::GetIsAiming()
{
	return bIsAiming;
}

#pragma region Health
/* Allow the UI to bind to Health and Stamina */ 
float ASyntyMixamoCharacter::GetHealth()
{
	return HealthPercentage;
}

/* Allor the UI to display Health as string values */ 
FText ASyntyMixamoCharacter::GetHealthIntText()
{
	int32 HP = FMath::RoundHalfFromZero(HealthPercentage * 100); //Maybe set a Full Health instead of 100
	FString HPS = FString::FromInt(HP);
	FString HealthHUD = HPS + FString(TEXT("%"));
	FText HPText = FText::FromString(HealthHUD);
	return HPText;
}

/* The PlayFlash function simply returns a bool that lets our UI know if it should play the red flash animation */ 
bool ASyntyMixamoCharacter::PlayFlash()
{
	if(redFlash)
	{
		redFlash = false;
		return true;
	}
	return false;
}

/* Recieve damages and update health */
//float ASyntyMixamoCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController * EventInstigator, AActor * DamageCauser)
//{
//	//bCanBeDamaged = false;
//	SetCanBeDamaged(false);
//
//	redFlash = true;
//	UpdateHealth(-DamageAmount);
//	DamageTimer();
//	return DamageAmount;
//}

void ASyntyMixamoCharacter::UpdateHealth(float HealthChange)
{
	Health = FMath::Clamp(Health += HealthChange, 0.0f, FullHealth);
	HealthPercentage = Health / FullHealth;
}
/* Toggle Invincibility state : The player has 2 secondes of invicibility after having been hit */ 
void ASyntyMixamoCharacter::SetDamageState()
{
	//bCanBeDamaged = true;
	SetCanBeDamaged(true);
}

void ASyntyMixamoCharacter::DamageTimer()
{
	GetWorldTimerManager().SetTimer(MemberTimerHandle, this, &ASyntyMixamoCharacter::SetDamageState, 2.0f, false);
	// NOTE : 2.f should be replaced by an editable variable
}
#pragma endregion Health

#pragma region Stamina
float ASyntyMixamoCharacter::GetStamina()
{
	return StaminaPercentage;
}

FText ASyntyMixamoCharacter::GetStaminaIntText()
{
	int32 SP = FMath::RoundHalfFromZero(StaminaPercentage * FullStamina);
	FString SPS = FString::FromInt(SP);
	FString FullSPS = FString::FromInt(FullStamina);
	FString StaminaHUD = SPS + FString(TEXT("/")) + FullSPS;
	FText MPText = FText::FromString(StaminaHUD);
	return MPText;
}

/* Use the TimelineCurve */
void ASyntyMixamoCharacter::SetStaminaValue()
{
	TimelineValue = MyTimeline->GetPlaybackPosition();
    CurveFloatValue = PreviousStamina + StaminaValue * StaminaCurve->GetFloatValue(TimelineValue);
	Stamina = FMath::Clamp(CurveFloatValue * FullHealth, 0.0f, FullStamina);
	StaminaPercentage = FMath::Clamp(CurveFloatValue, 0.0f, 1.0f);

}

void ASyntyMixamoCharacter::SetStaminaState()
{
	bCanUseStamina = true;
	StaminaValue = 0.0;
	//This is useless in the Molotov Game. Find another way to display the Stamina State
	// if(GunDefaultMaterial)
	// {
	// 	FP_Gun->SetMaterial(0, GunDefaultMaterial);
	// }
}

/* The final two functions are magic related and are similar to the health functions. UpdateMagic sets the Magic values and starts the timeline to get the player's magic meter back to 100%. UpdateMagic will run after 5 seconds of no magic firing to fill up the player's magic bar. SetMagicChange will toggle bCanUseMagic bool, set PreviousMagic, set MagicValue, change the gun's material to indicate overheating, the call PlayFromStart on the magic float timeline curve. */ 
void ASyntyMixamoCharacter::UpdateStamina()
{
	PreviousStamina = StaminaPercentage;
	StaminaPercentage = Stamina / FullStamina;
	StaminaValue = 1.0f;
	if(MyTimeline != nullptr) MyTimeline->PlayFromStart();
}

void ASyntyMixamoCharacter::SetStaminaChange(float StaminaChange)
{
	if (MyTimeline != nullptr) MyTimeline->Stop();

	//Timer before Stamina Regeneration
	GetWorldTimerManager().ClearTimer(StaminaTimerHandle);
	GetWorldTimerManager().SetTimer(StaminaTimerHandle, this, &ASyntyMixamoCharacter::UpdateStamina, 5.f, false);

	bCanUseStamina = false;
	PreviousStamina = StaminaPercentage;
	StaminaValue = (StaminaChange / FullStamina);

	//This is useless in the Molotov Game. Find another way to display the Stamina State
	// if(GunOverheatMaterial)
	// {
	// 	FP_Gun->SetMaterial(0, GunOverheatMaterial);
	// }

	if(MyTimeline != nullptr) MyTimeline->PlayFromStart();
}
#pragma endregion Stamina

void ASyntyMixamoCharacter::ThrowObjectChargeAttack()
{
	bIsAnimationBlended = true;

	float animTime = PlayAnimMontage(ThrowObject_Montage_Charge, 1.f, FName("StartCharge"));

    GetWorld()->GetTimerManager().SetTimer(ChargingThrowTimer, this, &ASyntyMixamoCharacter::ThrowObjectChargeLoop, 0.542f, false);
}

void ASyntyMixamoCharacter::ThrowObjectChargeLoop()
{
    GetWorldTimerManager().ClearTimer(ChargingThrowTimer);

	PlayAnimMontage(ThrowObject_Montage_ChargedLoop, 0.f, FName("ChargingLoop"));
}

void ASyntyMixamoCharacter::ThrowObjectReleaseAttack()
{
    GetWorldTimerManager().ClearTimer(ChargingThrowTimer);

	PlayAnimMontage(ThrowObject_Montage_Release, 1.5f, FName("ReleaseStart"));
}

void ASyntyMixamoCharacter::SpawnThrowableObjectInHandSocket()
{
	//if(MolotovActor == nullptr)
	//{
	//	FVector SpawnLocation = GetMesh()->GetSocketLocation(ThrowableObjectSocketName);
	//	FRotator SpawnRotation = GetActorRotation();

	//	/* This pointer doesn't exit anymore. Delete this when molotov is replaced by throwable */
	//	// SpawnedMolotov = GetWorld()->SpawnActor(SubClassMolotov, &SpawnLocation, &SpawnRotation);
	//	// SpawnedMolotov->AttachToComponent(GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, false), FName("MolotovSocket"));

	//	MolotovActor = Cast<AMolotovCoktail>(GetWorld()->SpawnActor(SubClassMolotov, &SpawnLocation, &SpawnRotation));
	//	MolotovActor->AttachToComponent(GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, false), ThrowableObjectSocketName);
	//}

	if (CurrentProjectile == nullptr)
	{
		FVector SpawnLocation = GetMesh()->GetSocketLocation(ThrowableObjectSocketName);
		FRotator SpawnRotation = GetActorRotation();

		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParams.Owner = this; 

		CurrentProjectile = Cast<AThrowableProjectile>(GetWorld()->SpawnActor(EquipedProjectile, &SpawnLocation, &SpawnRotation, SpawnParams));
		CurrentProjectile->AttachToComponent(GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, false), ThrowableObjectSocketName);

		GetCapsuleComponent()->IgnoreActorWhenMoving(CurrentProjectile, true);

		//int IgnoredObjects = GetCapsuleComponent()->MoveIgnoreActors.Num();
		//UE_LOG(LogTemp, Warning, TEXT("Ignored objects : %s"), *FString::FromInt(IgnoredObjects));
	}
}

void ASyntyMixamoCharacter::DeleteThrowableObjectFromHandSocket()
{
	////Supprimer l'objet coktail et les references
	//if (MolotovActor != nullptr)
	//{
	//	MolotovActor->Destroy();
	//	MolotovActor = nullptr;
	//}

	if (CurrentProjectile != nullptr)
	{
		//Stop Ignoring the object
		//GetCapsuleComponent()->IgnoreActorWhenMoving(CurrentProjectile, false);

		//Delete the object and reset the pointer
		CurrentProjectile->Destroy();
		CurrentProjectile = nullptr;
	}

	//Stop playing Throw animation
	UAnimMontage* Current = GetCurrentMontage();
	StopAnimMontage(Current);
}

void ASyntyMixamoCharacter::ThrowCoktailNotification()
{
	ThrowCurrentProjectile();
}

#pragma region Camera
void ASyntyMixamoCharacter::SetAimingCameraMode()
{
	CameraDesiredTargetArmLength = AimingCameraTargetArmLength;
	CameraDesiredSockedPosition = AimingCameraSocketPosition;

	EnableCharacterRotationTowardCamera();

	bIsAiming = true;
}

void ASyntyMixamoCharacter::SetNormalCameraMode()
{
	CameraDesiredTargetArmLength = NormalCameraTargetArmLength;	
	CameraDesiredSockedPosition = NormalCameraSocketPosition;

	DisableCharacterRotationTowardCamera();

	bIsAiming = false;
}

void ASyntyMixamoCharacter::EnableCharacterRotationTowardCamera()
{
	GetCharacterMovement()->bOrientRotationToMovement = false; 
	GetCharacterMovement()->bUseControllerDesiredRotation = true;
}

void ASyntyMixamoCharacter::DisableCharacterRotationTowardCamera()
{
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->bUseControllerDesiredRotation = false;
}

void ASyntyMixamoCharacter::SetDesiredCameraMode(float DeltaTime)
{
	//Target Arm Length Interpolation Init
	float InitialTargetArmLength = CameraBoom->TargetArmLength;

	//Socket Offset Interpolation Init
	FVector InitialSocketOffset = CameraBoom->SocketOffset;

	//Distance between both floats
	float DistanceBetweenFloats = FMath::Max(InitialTargetArmLength, CameraDesiredTargetArmLength) - FMath::Min(InitialTargetArmLength, CameraDesiredTargetArmLength);

	//Distance between both Vectors
	float DistanceBetweenVectors = FVector::Dist(InitialSocketOffset,CameraDesiredSockedPosition);

	//Calculate Vector Interpolation Speed, depending on desired Interpolation Speed
	float DesiredInterpolationSpeed = 500.f;
	float DesiredInterpolationSpeedVector = DistanceBetweenVectors * DesiredInterpolationSpeed / DistanceBetweenFloats;

	//Target Arm Length Interpolation
	if(InitialTargetArmLength != CameraDesiredTargetArmLength)
	{
		float NewTargetArmLength = FMath::FInterpConstantTo(InitialTargetArmLength, CameraDesiredTargetArmLength, DeltaTime, DesiredInterpolationSpeed);

		if(CameraBoom != nullptr)
		{
			CameraBoom->TargetArmLength = NewTargetArmLength;
		}
	}

	//Socket Offset Interpolation
	if (InitialSocketOffset != CameraDesiredSockedPosition)
	{
		FVector NewSocketOffset = FMath::VInterpConstantTo(InitialSocketOffset, CameraDesiredSockedPosition, DeltaTime, DesiredInterpolationSpeedVector);

		if(CameraBoom != nullptr)
		{
			CameraBoom->SocketOffset = NewSocketOffset;
		}
	}
}
#pragma endregion Camera