// Fill out your copyright notice in the Description page of Project Settings.

#include "Components/SplineComponent.h"
#include "Engine/Engine.h"
#include "GameFramework/Actor.h"
#include "DrawDebugHelpers.h"
#include "MolotovSplineActor.h"

// Sets default values
AMolotovSplineActor::AMolotovSplineActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SplineComp = CreateDefaultSubobject<USplineComponent>(TEXT("SplineComp"));
	RootComponent = SplineComp;
	SplineComp->bDrawDebug = true;
}

// Called when the game starts or when spawned
void AMolotovSplineActor::BeginPlay()
{
	Super::BeginPlay();

	IsEnabled = false;

}

// Called every frame
void AMolotovSplineActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}



FVector AMolotovSplineActor::DisplayCurve(FVector StartLocation, FVector EndLocation, float height)
{
	StartPosition = StartLocation; 
	Destination = EndLocation;
	Height = height; 

	DrawDebugPath();

	FLaunchDataa data = CalculateLaunchData();

	return data.InitialVelocity;
}

FLaunchDataa AMolotovSplineActor::CalculateLaunchData()
{
	//Aller chercher la gravite dans GetWorld()->GetGravityZ();
	Gravity = -980.0f;

	//C# float displacementY = target.position.y - ball.position.y;
	float DisplacementZ = Destination.Z - StartPosition.Z;

	//C# Vector3 displacementXZ = new Vector3 (target.position.x - ball.position.x, 0, target.position.z - ball.position.z);
	FVector DisplacementXY = FVector(Destination.X - StartPosition.X, Destination.Y - StartPosition.Y, 0);

	//C# float time = Mathf.Sqrt(-2*h/gravity) + Mathf.Sqrt(2*(displacementY - h)/gravity);
	float Time = FMath::Sqrt(-2 * Height / Gravity) + FMath::Sqrt(2 * (DisplacementZ - Height) / Gravity);

	//C# Vector3 velocityY = Vector3.up * Mathf.Sqrt (-2 * gravity * h);
	FVector VelocityZ = FVector::UpVector * FMath::Sqrt (-2 * Gravity * Height);

	//C# Vector3 velocityXZ = displacementXZ / time;
	FVector VelocityXY = DisplacementXY / Time; 

	//C# return new LaunchData(velocityXZ + velocityY * -Mathf.Sign(gravity), time);
	FLaunchDataa data;
	data.InitialVelocity = VelocityXY + VelocityZ * - FMath::Sign(Gravity);
	data.TimeToTarget = Time;  
	return data;
}

void AMolotovSplineActor::DrawDebugPath()
{
	//C# LaunchData launchData = CalculateLaunchData ();
	FLaunchDataa data = CalculateLaunchData();
	//C# Vector3 previousDrawPoint = ball.position;
	FVector PreviousDrawPoint = StartPosition;

	int Resolution = 30;
	for (int i = 0; i <= Resolution; i++)
	{
		//C# float simulationTime = i / (float)resolution * launchData.timeToTarget;
		float SimulationTime = i / (float)Resolution * data.TimeToTarget;

		//C# Vector3 displacement = launchData.initialVelocity * simulationTime + Vector3.up *gravity * simulationTime * simulationTime / 2f;
		FVector Displacement = data.InitialVelocity * SimulationTime + FVector::UpVector * Gravity * SimulationTime * SimulationTime / 2.f;

		//C# Vector3 drawPoint = ball.position + displacement;
		FVector DrawPoint = StartPosition + Displacement;
		
		//C# Debug.DrawLine (previousDrawPoint, drawPoint, Color.green);
		DrawDebugLine(GetWorld(), DrawPoint, PreviousDrawPoint, FColor::Red, false, 5.f, 0, 2);		
		//GEngine->AddOnScreenDebugMessage(i, 5.f, FColor::Yellow, FString::Printf(TEXT("Line %d, value %f"),i, SimulationTime));  

		//C# previousDrawPoint = drawPoint;
		PreviousDrawPoint = DrawPoint;
	}
}