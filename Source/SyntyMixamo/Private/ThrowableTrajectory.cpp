// Fill out your copyright notice in the Description page of Project Settings.


#include "ThrowableTrajectory.h"
#include "DrawDebugHelpers.h"

// Sets default values
AThrowableTrajectory::AThrowableTrajectory()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//PrimaryComponentTick.bCanEverTick = true;
	//PrimaryActorTick.bStartWithTickEnabled = true;
}

// Called when the game starts or when spawned
void AThrowableTrajectory::BeginPlay()
{
	Super::BeginPlay();
	
	IsTrajectoryEnabled = false;

}

// Called every frame
void AThrowableTrajectory::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
		if(GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5f, FColor::Green, FString::Printf(TEXT("AAAAAAA RN")));

	if(IsTrajectoryEnabled)
	{
		DrawTrajectoryDebugLine();
		if(GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5f, FColor::Green, FString::Printf(TEXT("ENABLED RN")));

	}

}

FVector AThrowableTrajectory::GetTrajectoryImpulse(FVector StartLocation, FVector EndLocation, float height)
{
	StartPosition = StartLocation; 
	Destination = EndLocation;
	Height = height; 

	LaunchData = CalculateLaunchData();

	return LaunchData.InitialVelocity;

}

FLaunchDataaa AThrowableTrajectory::CalculateLaunchData()
{
	//Aller chercher la gravite dans GetWorld()->GetGravityZ();
	Gravity = -980.0f;

	//C# float displacementY = target.position.y - ball.position.y;
	float DisplacementZ = Destination.Z - StartPosition.Z;

	//C# Vector3 displacementXZ = new Vector3 (target.position.x - ball.position.x, 0, target.position.z - ball.position.z);
	FVector DisplacementXY = FVector(Destination.X - StartPosition.X, Destination.Y - StartPosition.Y, 0);

	//C# float time = Mathf.Sqrt(-2*h/gravity) + Mathf.Sqrt(2*(displacementY - h)/gravity);
	float Time = FMath::Sqrt(-2 * Height / Gravity) + FMath::Sqrt(2 * (DisplacementZ - Height) / Gravity);

	//C# Vector3 velocityY = Vector3.up * Mathf.Sqrt (-2 * gravity * h);
	FVector VelocityZ = FVector::UpVector * FMath::Sqrt (-2 * Gravity * Height);

	//C# Vector3 velocityXZ = displacementXZ / time;
	FVector VelocityXY = DisplacementXY / Time; 

	//C# return new LaunchData(velocityXZ + velocityY * -Mathf.Sign(gravity), time);
	FLaunchDataaa data;
	data.InitialVelocity = VelocityXY + VelocityZ * - FMath::Sign(Gravity);
	data.TimeToTarget = Time;  
	return data;
}

void AThrowableTrajectory::DrawTrajectoryDebugLine()
{
	//C# Vector3 previousDrawPoint = ball.position;
	FVector PreviousDrawPoint = StartPosition;

	int Resolution = 30;
	for (int i = 0; i <= Resolution; i++)
	{
		//C# float simulationTime = i / (float)resolution * launchData.timeToTarget;
		float SimulationTime = i / (float)Resolution * LaunchData.TimeToTarget;

		//C# Vector3 displacement = launchData.initialVelocity * simulationTime + Vector3.up *gravity * simulationTime * simulationTime / 2f;
		FVector Displacement = LaunchData.InitialVelocity * SimulationTime + FVector::UpVector * Gravity * SimulationTime * SimulationTime / 2.f;

		//C# Vector3 drawPoint = ball.position + displacement;
		FVector DrawPoint = StartPosition + Displacement;
		
		//C# Debug.DrawLine (previousDrawPoint, drawPoint, Color.green);
		DrawDebugLine(GetWorld(), DrawPoint, PreviousDrawPoint, FColor::Green, false, 5.f, 0, 5);		
		DrawDebugSphere(GetWorld(), DrawPoint, 16.f, 4, FColor::Green, false, -1.f, 0, 1.f );

		//C# previousDrawPoint = drawPoint;
		PreviousDrawPoint = DrawPoint;
	}
}

void AThrowableTrajectory::SetDisplayTrajectory(bool enabled)
{
	IsTrajectoryEnabled = enabled;

	if (IsTrajectoryEnabled)
	{
		if(GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5f, FColor::Black, FString::Printf(TEXT("OUI")));

	}
	else 
	{
		if(GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5f, FColor::Black, FString::Printf(TEXT("NON")));

	}

} 