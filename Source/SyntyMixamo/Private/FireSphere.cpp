// Fill out your copyright notice in the Description page of Project Settings.


#include "FireSphere.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "TimerManager.h"

// Sets default values
AFireSphere::AFireSphere()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	SphereComponent->InitSphereRadius(10.f);
	SphereComponent->SetSimulatePhysics(true);
	SphereComponent->SetCollisionProfileName(TEXT("BlockAllDynamic"));
	SphereComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	RootComponent = SphereComponent; 

	FireEmitter = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("FireEffect"));
	FireEmitter->SetupAttachment(RootComponent);
	FireEmitter->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
    static ConstructorHelpers::FObjectFinder<UParticleSystem> FireEmitterAsset(TEXT("/Game/PolygonScifi/Particles/FX_Fire.FX_Fire"));
	if(FireEmitterAsset.Succeeded())
	{
		FireEmitter->SetTemplate(FireEmitterAsset.Object);
	}

	IsExtincting = false;

    SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &AFireSphere::OnOverlapBegin);
    SphereComponent->OnComponentEndOverlap.AddDynamic(this, &AFireSphere::OnOverlapEnd);

    bCanApplyDamage = false;
}

void AFireSphere::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
    if ( (OtherActor != nullptr ) && (OtherActor != this) && ( OtherComp != nullptr ) )
    {
        bCanApplyDamage = true;
        MyCharacter = Cast<AActor>(OtherActor);
        MyHit = SweepResult;
        GetWorldTimerManager().SetTimer(FireTimerHandle, this, &AFireSphere::ApplyFireDamage, 2.2f, true, 0.0f);
    }
}

void AFireSphere::OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
    bCanApplyDamage = false;
    GetWorldTimerManager().ClearTimer(FireTimerHandle);
}

void AFireSphere::ApplyFireDamage()
{
    if(bCanApplyDamage)
    {
        UGameplayStatics::ApplyPointDamage(MyCharacter, 200.0f, GetActorLocation(), MyHit, nullptr, this, FireDamageType);
    }
}

// Called when the game starts or when spawned
void AFireSphere::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFireSphere::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(IsExtincting == true)
	{
		FVector ActualVector = FireEmitter->GetRelativeScale3D();

		if(ActualVector.X >= 0)
		{
			FireEmitter->SetRelativeScale3D(ActualVector - FVector::OneVector * 0.1f);
		}
		else
		{
			ExtinctionEnd();
		}
	}
}

void AFireSphere::SetFlameSize(float Size)
{
	SphereComponent->InitSphereRadius(10.f * Size * Size);

	FireEmitter->SetRelativeScale3D(FVector::OneVector * Size);

	GetWorldTimerManager().SetTimer(ExtinctionStartTimer, this, &AFireSphere::ExtinctionStart, 1, false);
}

void AFireSphere::ExtinctionStart()
{
	IsExtincting = true;
}

void AFireSphere::ExtinctionEnd()
{
	Destroy();
}