// Fill out your copyright notice in the Description page of Project Settings.


#include "ThrowableProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/RotatingMovementComponent.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AThrowableProjectile::AThrowableProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("SphereComp"));
	CollisionComp->SetCollisionProfileName("BlockAllDynamic");
	CollisionComp->OnComponentHit.AddDynamic(this, &AThrowableProjectile::OnHit);	// set up a notification for when this component hits something blocking
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;
	RootComponent = CollisionComp;

	StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	StaticMeshComp->SetGenerateOverlapEvents(false);
	StaticMeshComp->SetCollisionProfileName("NoCollision");
	StaticMeshComp->SetupAttachment(CollisionComp);

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovementComp = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovementComp->UpdatedComponent = CollisionComp;
	ProjectileMovementComp->InitialSpeed = 3000.f;
	ProjectileMovementComp->MaxSpeed = 3000.f;
	ProjectileMovementComp->bRotationFollowsVelocity = true;
	ProjectileMovementComp->bShouldBounce = true;
	ProjectileMovementComp->SetAutoActivate(false);

	RotatingMovementComp = CreateDefaultSubobject<URotatingMovementComponent>(TEXT("RotatingComp"));
	RotatingMovementComp->UpdatedComponent = CollisionComp;
	RotatingMovementComp->SetAutoActivate(false);	
}

// Called when the game starts or when spawned
void AThrowableProjectile::BeginPlay()
{
	Super::BeginPlay();

	if (GetOwner() != nullptr)
	{
		CollisionComp->IgnoreActorWhenMoving(GetOwner(), true);
	}
}

// Called every frame
void AThrowableProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AThrowableProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	UE_LOG(LogTemp, Warning, TEXT("Parent class OnHit"));

	CollisionComp->SetSimulatePhysics(false);

	ProjectileMovementComp->Deactivate();
	RotatingMovementComp->Deactivate();
}

void AThrowableProjectile::ThrowProjectile(float ThrowForce, FVector ThrowDirection,  FRotator StartRotation, FVector InstigatorVelocity)
{
	SetActorRotation(StartRotation);

	float ForwardRotationForce = -1000.f;
	float SideRotation = FQuat::FindBetweenVectors(ThrowDirection, InstigatorVelocity).Rotator().Yaw;
	FRotator DesiredRot = FRotator(ForwardRotationForce, 0.f, SideRotation * 10);
	RotatingMovementComp->RotationRate = DesiredRot;
	RotatingMovementComp->Activate();

	ProjectileMovementComp->Velocity = ThrowDirection * ThrowForce + InstigatorVelocity * 500;
	ProjectileMovementComp->Activate();
}

void AThrowableProjectile::ProjectileCollision()
{
	UE_LOG(LogTemp, Warning, TEXT("Parent class ProjectileCollision"));

}
