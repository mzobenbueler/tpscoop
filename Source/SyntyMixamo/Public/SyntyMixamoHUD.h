// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SyntyMixamoHUD.generated.h"

/**
 * 
 */
UCLASS()
class SYNTYMIXAMO_API ASyntyMixamoHUD : public AHUD
{
	GENERATED_BODY()
	
public: 

	ASyntyMixamoHUD();

	//virtual void DrawHUD() override;

	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, Category = "Health")
	TSubclassOf<class UUserWidget> HUDWidgetClass;

	UPROPERTY(EditAnywhere, Category = "Health")
	class UUserWidget* CurrentWidget; 
};
