// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ThrowableTrajectory.h"
#include "Components/TimelineComponent.h"
#include "Components/BoxComponent.h"
#include "SyntyMixamoCharacter.generated.h"

class UCameraComponent;
class USpringArmComponent;
class AThrowableProjectile;
class UAnimMontage;

UCLASS(config=Game)
class ASyntyMixamoCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FollowCamera;

public:
	ASyntyMixamoCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	/* Specific animations */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animation", meta = (AllowPrivateAccess = "true"))
	UAnimMontage* ThrowObject_Montage_Charge;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animation", meta = (AllowPrivateAccess = "true"))
	UAnimMontage* ThrowObject_Montage_ChargedLoop;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animation", meta = (AllowPrivateAccess = "true"))
	UAnimMontage* ThrowObject_Montage_Release;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animation", meta = (AllowPrivateAccess = "true"))
	UAnimMontage* RollForward_Montage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animation", meta = (AllowPrivateAccess = "true"))
	UAnimMontage* SprintForward_Montage;

	/* Throwable Object */
	//Replace molotov by throwable
	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> SubClassMolotov;

	UPROPERTY(EditAnywhere)
	class AMolotovCoktail* MolotovActor;

	UPROPERTY(EditAnywhere, Category="Projectile")
	TSubclassOf<AThrowableProjectile> EquipedProjectile;

	UPROPERTY(EditAnywhere, Category="Projectile")
	AThrowableProjectile* CurrentProjectile;

	void SpawnThrowableObjectInHandSocket();

	void DeleteThrowableObjectFromHandSocket();

	void ThrowCoktailNotification();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, Category = "Aim")
	float AimDistanceMax = 1500.f;

	/** boolean that tells us if we need to branch our animation blueprint paths */
	UFUNCTION(BlueprintCallable, Category = "Animation")
	bool GetIsAnimationBlended();

	/** boolean that tells us if we are SPRINTING */
	UFUNCTION(BlueprintCallable, Category = "Animation")
	bool GetIsSprinting();

	/** boolean that tells us if we are AIMING */
	UFUNCTION(BlueprintCallable, Category = "Animation")
	bool GetIsAiming();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Trajectory")
	FVector MolotovThrowRelativeLocation;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Trajectory", meta = (AllowPrivateAccess = "true"))
	class USceneComponent* ThrowLocation;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Throwing", meta = (AllowPrivateAccess = "true"))
	float ThrowDistanceMax;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Throwing", meta = (AllowPrivateAccess = "true"))
	float ThrowDistanceMin;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Throwing", meta = (AllowPrivateAccess = "true"))
	float ThrowHeightMax;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Throwing", meta = (AllowPrivateAccess = "true"))
	float ThrowHeightMin;

private: 

	//Move this in a new class
	void CalculateCharacterAimLocation();

	void GetThrowableTrajectory();

protected:

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

protected:

	bool bIsAiming;

	bool bIsAnimationBlended;

	bool bIsSprinting;

	/* PLAYER INPUTS */
	/** Attack input */ 
	void Attack();

	/** Roll input */ 
	void Roll();

	/** Sprint input */ 
	void SprintStart();

	void SprintStop();

	//Enable and disable aiming
	void StartAimMode();

	void StopAimMode();

	//Throwing Object by clicking Left Mouse
	void ThrowObjectStart();

	void ThrowObjectChargeAttack();

	void ThrowObjectReleaseAttack();

	void ThrowObjectChargeLoop();

	void TimerEnds();

	//Delai avant de passer l'animation en boucle
	//Non-Gameplay related
    FTimerHandle ChargingThrowTimer;

	//Timers for attacking without aiming
	//Delay before playing throw animation
	//Gameplay related
	FTimerHandle DelayBeforeThrowTimer;
	//Timer before reseting character orientation to normal
	FTimerHandle DelayBeforeResetingCharacterOrientation;

	/** Throw Trajectory */ 
	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> SubClassTrajectory;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ThrowTrajectory", meta = (AllowPrivateAccess = "true"))
	class UThrowTrajectoryComp* ThrowTrajectory;

	UPROPERTY(VisibleAnywhere)
	FVector ThrowImpulse; 

	//Aim Location
	FVector AimLocation;

	FVector AimLocationSelected;

	/* Players Camera things */
	void SetDesiredCameraMode(float DeltaTime);
	float CameraDesiredTargetArmLength;
	FVector CameraDesiredSockedPosition;
	void SetAimingCameraMode();
	void SetNormalCameraMode();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
	float AimingCameraTargetArmLength;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
	FVector AimingCameraSocketPosition;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
	float NormalCameraTargetArmLength;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
	FVector NormalCameraSocketPosition;

	void EnableCharacterRotationTowardCamera();
	void DisableCharacterRotationTowardCamera();

	UPROPERTY(VisibleDefaultsOnly, Category="ThrowableObject")
	FName ThrowableObjectSocketName;

	void ThrowCurrentProjectile();

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

	/* Health and Stamina thing */
	//virtual float TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController * EventInstigator, AActor * DamageCauser);

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	/* TUTO Health and Magic from Harrisson1 */

	//Health things
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	float FullHealth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	float Health;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	float HealthPercentage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	bool redFlash;

	//Stamina things
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
	float FullStamina;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
	float Stamina;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
	float StaminaPercentage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
	float PreviousStamina;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
	float StaminaValue;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
	UCurveFloat* StaminaCurve;

	float CurveFloatValue; 
	float TimelineValue; 
	bool bCanUseStamina;

	UTimelineComponent* MyTimeline; 
	FTimerHandle MemberTimerHandle;
	FTimerHandle StaminaTimerHandle;

	/** Get Health */
	UFUNCTION(BlueprintPure, Category = "Health")
	float GetHealth();	

	/** Update Health */
	UFUNCTION(BlueprintCallable, Category = "Health")
	void UpdateHealth(float HealthChange);

	/** Get Health Text */
	UFUNCTION(BlueprintPure, Category = "Health")
	FText GetHealthIntText();

	/** Get Stamina */
	UFUNCTION(BlueprintPure, Category = "Stamina")
	float GetStamina();

	/** Update Stamina */
	UFUNCTION(BlueprintCallable, Category = "Stamina")
	void UpdateStamina();

	/** Get Stamina Text */
	UFUNCTION(BlueprintPure, Category = "Stamina")
	FText GetStaminaIntText();

	/** Set Magic Value */
	UFUNCTION()
	void SetStaminaValue();

	/** Set Damage State */
	UFUNCTION()
	void SetStaminaState();

	/** Set Stamina State */
	UFUNCTION()
	void SetStaminaChange(float StaminaChange);
	
	/** Play Flash */
	UFUNCTION(BlueprintPure, Category = "Health")
	bool PlayFlash();

	/** Voir si ca sert */
	UPROPERTY(EditAnywhere, Category = "Magic")
	class UMaterialInterface* GunDefaultMaterial;

	/** Voir si ca sert */
	UPROPERTY(EditAnywhere, Category = "Magic")
	class UMaterialInterface* GunOverheatMaterial;

	/** Voir si ca sert */
	//void ReceivePointDamage(float Damage, const UDamageType * DamageType, FVector HitLocation, FVector HitNormal, UPrimitiveComponent * HitComponent, FName BoneName, FVector ShotFromDirection, AController * InstigatedBy, AActor * DamageCauser, const FHitResult & HitInfo);

	/** Set Damage State */
	UFUNCTION()
	void SetDamageState();

	/** Damage Timer */
	UFUNCTION()
	void DamageTimer();


};

