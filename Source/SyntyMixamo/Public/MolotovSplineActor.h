// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MolotovSplineActor.generated.h"


USTRUCT(BlueprintType)
struct FLaunchDataa
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector InitialVelocity; 

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float TimeToTarget;

	// Default constructor
	FLaunchDataa()
	{
		InitialVelocity = FVector::ZeroVector;
		TimeToTarget = 0.f;
	}
};

UCLASS()
class SYNTYMIXAMO_API AMolotovSplineActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMolotovSplineActor();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SplineController")
	class USplineComponent* SplineComp;

	// UPROPERTY(EditAnywhere, Category = "SplineController")
	// FVector Displacement; 

	UPROPERTY(EditAnywhere, Category = "SplineController")
	FVector InitialVelocity; 

	UPROPERTY(EditAnywhere, Category = "SplineController")
	float FinalVelocity; 

	UPROPERTY(EditAnywhere, Category = "SplineController")
	float Acceleration; 

	// UPROPERTY(EditAnywhere, Category = "SplineController")
	// float Time; 

	UPROPERTY(EditAnywhere, Category = "SplineController")
	float Height; 

	float Gravity; 

	UPROPERTY(EditAnywhere, Category = "SplineController")
	FVector StartPosition; 

	UPROPERTY(EditAnywhere, Category = "SplineController")
	FVector Destination; 

	FVector DisplayCurve(FVector StartLocation, FVector EndLocation, float height);

	void DeleteSpline();

	bool IsEnabled = false;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private: 
	float StartTime;

	FLaunchDataa CalculateLaunchData();

	void DrawDebugPath();

};