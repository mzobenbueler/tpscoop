// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ThrowTrajectoryComp.generated.h"

USTRUCT(BlueprintType)
struct FLaunchData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector InitialVelocity; 

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float TimeToTarget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Height;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector Displacement;

	// Default constructor
	FLaunchData()
	{
		InitialVelocity = FVector::ZeroVector;
		TimeToTarget = 0.f;
		Height = 0.f;
		Displacement = FVector::ZeroVector;
	}
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SYNTYMIXAMO_API UThrowTrajectoryComp : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UThrowTrajectoryComp();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	/** Functions called from the character */
	UFUNCTION(BlueprintCallable)
	FVector GetTrajectoryImpulse(FVector StartLocation, FVector EndLocation, float height);

	UFUNCTION(BlueprintCallable)
	float GetTrajectoryDisplacement(FVector StartLocation, FVector InitialForce, FVector InitialDirection, float Height);

	UFUNCTION(BlueprintCallable)
	void SetDisplayTrajectory(bool enabled); 


private:

	UFUNCTION(BlueprintCallable)
	FLaunchData CalculateLaunchDataFromLocations(FVector StartLocation, FVector EndLocation, float Height);

	UFUNCTION(BlueprintCallable)
	FLaunchData CalculateLaunchDataFromInitialForce(FVector InitialForce, float Height);

	UFUNCTION(BlueprintCallable)
	void DrawTrajectoryDebugLine();

	UPROPERTY(VisibleAnywhere, Category = "Trajectory")
	FVector StartPosition; 

	UPROPERTY(VisibleAnywhere, Category = "Trajectory")
	FVector Destination;

	// UPROPERTY(EditAnywhere, Category = "Trajectory")
	// float Height; 

	UPROPERTY(EditAnywhere, Category = "Trajectory")
	bool IsTrajectoryEnabled = false;

	UPROPERTY(VisibleAnywhere, Category = "Trajectory")
	float Gravity; 

	FLaunchData LaunchData;

};
