// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/CapsuleComponent.h"
#include "Components/StaticMeshComponent.h"
#include "MolotovCoktail.generated.h"

UCLASS()
class SYNTYMIXAMO_API AMolotovCoktail : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMolotovCoktail();	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void EnableExplosion();

	void Explode();

	UFUNCTION()
	void OnCompHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	bool ExplostionEnabled; 

	UCapsuleComponent* CapsuleComp;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* BottleMesh; 

	UFUNCTION()
	void EnableMolotovPhysicsAndThrow(FVector Direction, float Force, FVector RotationDir);

	UPROPERTY(VisibleAnywhere)
	UParticleSystemComponent* FireEmitter;

	UFUNCTION()
	void GenerateFireExplosionFX();

	FTimerHandle FlameOneTimer;
	FTimerHandle FlameTwoTimer;
	FTimerHandle FlameThreeTimer;

	UFUNCTION()
	void GenerateFireFlame(float Scale, bool IsLastFlame);

	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> FireSphereActor;

};
