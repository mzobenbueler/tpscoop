// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ThrowableProjectile.generated.h"

class UProjectileMovementComponent;
class UCapsuleComponent;
class URotatingMovementComponent;
class UStaticMeshComponent;

UCLASS()
class SYNTYMIXAMO_API AThrowableProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AThrowableProjectile();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Projectile")
	UCapsuleComponent* CollisionComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Projectile")
	UStaticMeshComponent* StaticMeshComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement")
	UProjectileMovementComponent* ProjectileMovementComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement")
	URotatingMovementComponent* RotatingMovementComp;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	bool bRotatingEnabled;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/** called when projectile hits something */
	UFUNCTION()
	virtual void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	/** Returns CollisionComp subobject **/
	UCapsuleComponent* GetCollisionComp() const { return CollisionComp; }

	/** Returns ProjectileMovement subobject **/
	UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovementComp; }

	UFUNCTION()
	void ThrowProjectile(float Force, FVector Direction,  FRotator StartRotation, FVector InstigatorVelocity);

	UFUNCTION()
	virtual void ProjectileCollision();


};
