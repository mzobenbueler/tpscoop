// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ThrowableProjectile.h"
#include "ProjectileMolotov.generated.h"

class AFireExplosion;
/**
 * 
 */
UCLASS()
class SYNTYMIXAMO_API AProjectileMolotov : public AThrowableProjectile
{
	GENERATED_BODY()

public: 

	AProjectileMolotov();

protected: 

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Explosion")
	TSubclassOf<AFireExplosion> ExplosionClass;

	UPROPERTY(EditDefaultsOnly, Category = "FireParticle")
	UParticleSystemComponent* FireEmitter;

	FTimerHandle FTimerHandle_BeforeDeactivation;

	UPROPERTY(EditDefaultsOnly, Category = "FireParticle")
	float DelayBeforeDestroyingActor;

public:

	/** called when projectile hits something */
	virtual void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;
};
