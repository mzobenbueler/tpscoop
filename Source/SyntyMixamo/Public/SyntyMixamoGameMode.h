// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "SyntyMixamoCharacter.h"
#include "GameFramework/GameModeBase.h"
#include "SyntyMixamoGameMode.generated.h"

UENUM()
enum class EGamePlayState
{
	EPlaying, 
	EGameOver, 
	EUnknown
};

UCLASS(minimalapi)
class ASyntyMixamoGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASyntyMixamoGameMode();

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	ASyntyMixamoCharacter* MyCharacter;

	/* Returns the current playing state */
	UFUNCTION(BlueprintPure, Category = "Health")
	EGamePlayState GetCurrentState() const;

	/* Sets a new playing state */
	void SetCurrentState(EGamePlayState NewState);

	/** The Widget class to use HUD screen */
	UPROPERTY(EditAnywhere, Category = "Health")
	TSubclassOf<class UUserWidget> HUDWidgetClass;

	/** The Widget Instant of the HUD */
	UPROPERTY(EditAnywhere, Category = "Health")
	class UUserWidget* CurrentWidget;

private: 

	/* Keeps track of the current playing state */
	EGamePlayState CurrentState;

	/* Handle any function calls that ely upon changing the playing state of our game */
	void HandleNewState(EGamePlayState NewState); 

};



