// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FireExplosion.generated.h"

class UParticleSystemComponent; 
class USphereComponent;

UCLASS()
class SYNTYMIXAMO_API AFireExplosion : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFireExplosion();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
	USphereComponent* SphereComp; 

	UPROPERTY(EditDefaultsOnly, Category = "FireParticle")
	float FireScale;

	UPROPERTY(EditDefaultsOnly, Category = "FireParticle")
	UParticleSystemComponent* FireEmitter;

	FTimerHandle FTimerHandle_BeforeDeactivation; 
	FTimerHandle FTimerHandle_BeforeDestoryingActor;

	void StartExplosion();

	void EndExplosion(); 

	bool bExplosionEnding;

	UPROPERTY(EditAnywhere, Category = "FireParticle")
	float DelayBeforeExtinction;

	UPROPERTY(EditDefaultsOnly, Category = "FireParticle")
	float DelayBeforeDestroyingActor;

	bool bCanDamageActor;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UDamageType> FireDamageType;

};
