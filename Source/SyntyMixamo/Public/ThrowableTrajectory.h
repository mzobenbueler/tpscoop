// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ThrowableTrajectory.generated.h"

USTRUCT(BlueprintType)
struct FLaunchDataaa
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector InitialVelocity; 

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float TimeToTarget;

	// Default constructor
	FLaunchDataaa()
	{
		InitialVelocity = FVector::ZeroVector;
		TimeToTarget = 0.f;
	}
};

UCLASS()
class SYNTYMIXAMO_API AThrowableTrajectory : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AThrowableTrajectory();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/** Functions called from the character */
	UFUNCTION(BlueprintCallable)
	FVector GetTrajectoryImpulse(FVector StartLocation, FVector EndLocation, float height);

	UFUNCTION(BlueprintCallable)
	void SetDisplayTrajectory(bool enabled); 

private:

	UFUNCTION(BlueprintCallable)
	FLaunchDataaa CalculateLaunchData();

	UFUNCTION(BlueprintCallable)
	void DrawTrajectoryDebugLine();

	UPROPERTY(VisibleAnywhere, Category = "Trajectory")
	FVector StartPosition; 

	UPROPERTY(VisibleAnywhere, Category = "Trajectory")
	FVector Destination;

	UPROPERTY(EditAnywhere, Category = "Trajectory")
	float Height; 

	UPROPERTY(EditAnywhere, Category = "Trajectory")
	bool IsTrajectoryEnabled = false;

	UPROPERTY(VisibleAnywhere, Category = "Trajectory")
	float Gravity; 

	FLaunchDataaa LaunchData;

};
